package com.example.miguel.tictactoe_mb_jm;

import android.app.Activity;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.MalformedURLException;


public class MainActivity extends Activity {

    private EditText[] board = new EditText[9];


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        for (int i = 0 ; i<3 ; i++)
        {
            for(int j = 0 ; j<3 ; j++)
            {
                int resID = getResources().getIdentifier("editText_"+ Integer.toString(i) +"_"+ Integer.toString(j),
                        "id", getPackageName());
                board[i*3+j] = (EditText)findViewById(resID);
            }
        }

        final Button sendButton = (Button)findViewById(R.id.button_send);
        final Button refreshButton = (Button)findViewById(R.id.button_refresh);

        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                String[] t = {"POST","http://gg-wp-easynoob.appspot.com/script?player_1=jean&player_2=miguel&board="};
                new HttpGetTask(t).execute(t);
            }
        });

        refreshButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String[] t = {"GET","http://gg-wp-easynoob.appspot.com/"};

                new HttpGetTask(t).execute(t);
            }
        });


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    private class HttpGetTask extends AsyncTask<String, Void, String> {

        private static final String TAG = "HttpGetTask";
        private String BOARD;
        private String URL;
        private String TYPE_REQUEST;


        public HttpGetTask(String[] args)
        {
            TYPE_REQUEST = args[0];
            URL = args[1];
            Log.e("INFO", TYPE_REQUEST+" "+ URL);
        }

        @Override
        protected String doInBackground(String... params) {
            String _TYPE_REQUEST = params[0];
            String _URL = params[1];
            String data = "";
            HttpURLConnection httpUrlConnection = null;

            if(TYPE_REQUEST.equals("POST"))
            {
                for (int i = 0 ; i<3 ; i++)
                {
                    for (int j = 0 ; j<3 ; j++)
                    {
                        String str_temp = board[i*3+j].getText().toString();
                        if(str_temp.length()==0)
                        {
                            URL+="-";
                        }
                        else
                        {
                            URL+=str_temp;
                        }

                    }
                }
            }

            try {
                httpUrlConnection = (HttpURLConnection) new URL(URL).openConnection();
                InputStream in = new BufferedInputStream(httpUrlConnection.getInputStream());
                if(TYPE_REQUEST.equals("POST"))
                {
                    return "";
                }
                data = readStream(in);

            } catch (MalformedURLException exception) {
                Log.e(TAG, "MalformedURLException");
            } catch (IOException exception) {
                Log.e(TAG, "IOException");
            } finally {
                if (null != httpUrlConnection)
                    httpUrlConnection.disconnect();
            }
            return data;
        }

        @Override
        protected void onPostExecute(String result) {
            Log.e("INFO RESULT", result);
            if (result.length()==0)
            {
                return;
            }
            String b = result.split(",")[2].split(":")[1];
            for (int i = 0 ; i<b.length() ; i++)
            {
                board[i].setText(Character.toString(b.charAt(i)));
            }
        }

        private String readStream(InputStream in) {
            BufferedReader reader = null;
            StringBuffer data = new StringBuffer("");
            try {
                reader = new BufferedReader(new InputStreamReader(in));
                String line = "";
                while ((line = reader.readLine()) != null) {
                    data.append(line);
                }
            } catch (IOException e) {
                Log.e(TAG, "IOException");
            } finally {
                if (reader != null) {
                    try {
                        reader.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
            return data.toString();
        }
    }
}
